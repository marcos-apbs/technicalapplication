﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using API.Models;
using DAL.Models;
using DAL.Storage;

namespace API.Controllers
{
    [Authorize]
    public class LocalsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Locals
        public IQueryable<Local> GetLocals()
        {
            return db.Locals.Include(v => v.Venues);
        }

        // GET: api/Locals/5
        [ResponseType(typeof(Local))]
        public async Task<IHttpActionResult> GetLocal(Guid id)
        {
            Local local = await db.Locals.FindAsync(id);
            if (local == null)
            {
                return NotFound();
            }

            return Ok(local);
        }

        // PUT: api/Locals/5
        [ResponseType(typeof(void))]
        [Authorize(Roles = "Admin")]
        public async Task<IHttpActionResult> PutLocal(Guid id, Local local)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != local.Id)
            {
                return BadRequest();
            }
            //db.Entry(local).State = EntityState.Modified;
                        
            try
            {
                //TLocal Insert Exemple
                TLocal db2 = new TLocal();
                db2.Insert(local);

                //await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LocalExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Locals
        [ResponseType(typeof(Local))]
        [Authorize(Roles = "Admin")]
        public async Task<IHttpActionResult> PostLocal(Local local)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Locals.Add(local);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (LocalExists(local.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = local.Id }, local);
        }

        // DELETE: api/Locals/5
        [ResponseType(typeof(Local))]
        [Authorize(Roles = "Admin")]
        public async Task<IHttpActionResult> DeleteLocal(Guid id)
        {
            Local local = await db.Locals.FindAsync(id);
            if (local == null)
            {
                return NotFound();
            }

            db.Locals.Remove(local);
            await db.SaveChangesAsync();

            return Ok(local);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LocalExists(Guid id)
        {
            return db.Locals.Count(e => e.Id == id) > 0;
        }
    }
}