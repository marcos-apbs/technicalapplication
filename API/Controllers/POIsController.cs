﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using API.Models;
using DAL.Models;

namespace API.Controllers
{
    [Authorize]
    public class POIsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/POIs
        public IQueryable<POI> GetPOIs()
        {
            return db.POIs.Include(l => l.Local.Venues);
        }

        // GET: api/POIs/5
        [ResponseType(typeof(POI))]
        public async Task<IHttpActionResult> GetPOI(Guid id)
        {
            POI pOI = await db.POIs.FindAsync(id);
            if (pOI == null)
            {
                return NotFound();
            }

            return Ok(pOI);
        }

        // PUT: api/POIs/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPOI(Guid id, POI pOI)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pOI.Id)
            {
                return BadRequest();
            }

            db.Entry(pOI).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!POIExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/POIs
        [ResponseType(typeof(POI))]
        public async Task<IHttpActionResult> PostPOI(POI pOI)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.POIs.Add(pOI);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (POIExists(pOI.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = pOI.Id }, pOI);
        }

        // DELETE: api/POIs/5
        [ResponseType(typeof(POI))]
        public async Task<IHttpActionResult> DeletePOI(Guid id)
        {
            POI pOI = await db.POIs.FindAsync(id);
            if (pOI == null)
            {
                return NotFound();
            }

            db.POIs.Remove(pOI);
            await db.SaveChangesAsync();

            return Ok(pOI);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool POIExists(Guid id)
        {
            return db.POIs.Count(e => e.Id == id) > 0;
        }
    }
}