﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using API.Models;
using DAL.Models;
using Microsoft.AspNet.Identity;

namespace API.Controllers
{
    [Authorize]
    public class VenuesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Venues
        public IQueryable<Venue> GetVenues()
        {
            //Exemplo de LINQ to SQL
            var model = (from p in db.Venues
                         join u in db.Locals on p.LocalID equals u.Id
                         select p).Include("Local");
            return model;
        }

        // GET: api/Venues/5
        [ResponseType(typeof(Venue))]
        public async Task<IHttpActionResult> GetVenue(Guid id)
        {
            Venue venue = await db.Venues.FindAsync(id);
            if (venue == null)
            {
                return NotFound();
            }

            return Ok(venue);
        }

        // PUT: api/Venues/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutVenue(Guid id, Venue venue)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != venue.Id)
            {
                return BadRequest();
            }

            if (User.Identity.GetUserId() == venue.OwnerID || User.IsInRole("Admin"))
            {
                venue.OwnerID = db.Venues.Find(id).OwnerID;
                db.Entry(venue).State = EntityState.Modified;

                try
                {
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!VenueExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                return BadRequest();
            }
        }

        // POST: api/Venues
        [ResponseType(typeof(Venue))]
        public async Task<IHttpActionResult> PostVenue(Venue venue)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            venue.OwnerID = User.Identity.GetUserId();
            db.Venues.Add(venue);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (VenueExists(venue.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = venue.Id }, venue);
        }

        // DELETE: api/Venues/5
        [ResponseType(typeof(Venue))]
        public async Task<IHttpActionResult> DeleteVenue(Guid id)
        {
            Venue venue = await db.Venues.FindAsync(id);
            if (venue == null)
            {
                return NotFound();
            }

            db.Venues.Remove(venue);
            await db.SaveChangesAsync();

            return Ok(venue);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool VenueExists(Guid id)
        {
            return db.Venues.Count(e => e.Id == id) > 0;
        }
    }
}