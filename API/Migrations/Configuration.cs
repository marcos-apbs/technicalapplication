namespace API.Migrations
{
    using API.Models;
    using DAL.Models;
    using FizzWare.NBuilder;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<API.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(API.Models.ApplicationDbContext context)
        {
            if (!context.Roles.Any(r => r.Name == "Admin"))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole { Name = "Admin" };

                manager.Create(role);
            }

            if (!context.Roles.Any(r => r.Name == "Customer"))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole { Name = "Customer" };

                manager.Create(role);
            }

            if (!context.Users.Any(u => u.UserName == "Founder"))
            {
                var store = new UserStore<ApplicationUser>(context);
                var manager = new UserManager<ApplicationUser>(store);
                var user = new ApplicationUser { UserName = "Founder" };

                manager.Create(user, "foobar");
                manager.AddToRole(user.Id, "Admin");
            }

            if (!context.Users.Any(u => u.UserName == "Client1"))
            {
                var store = new UserStore<ApplicationUser>(context);
                var manager = new UserManager<ApplicationUser>(store);
                var user = new ApplicationUser { UserName = "Client1" };

                manager.Create(user, "foobar");
                manager.AddToRole(user.Id, "Customer");
            }

            if (!context.Users.Any(u => u.UserName == "Client2"))
            {
                var store = new UserStore<ApplicationUser>(context);
                var manager = new UserManager<ApplicationUser>(store);
                var user = new ApplicationUser { UserName = "Client2" };

                manager.Create(user, "foobar");
                manager.AddToRole(user.Id, "Customer");
            }

            if (!context.Locals.Any() || !context.POIs.Any() || !context.Venues.Any())
            {
                //Local Seed
                var locals = Builder<Local>.CreateListOfSize(50)
                .All()
                .With(c => c.Name = Faker.Address.City())
                .With(c => c.GPS_Lat = Faker.RandomNumber.Next(-90, 90))
                .With(c => c.GPS_Long = Faker.RandomNumber.Next(-180, 180))
                .Build();

                context.Locals.AddOrUpdate(c => c.Id, locals.ToArray());

                //POI Seed
                var pois = Builder<POI>.CreateListOfSize(50)
                .All()
                .With(c => c.Name = Faker.Address.StreetName())
                .With(c => c.Description = Faker.Lorem.Paragraph(5))
                .With(c => c.Local = Pick<Local>.RandomItemFrom(locals))
                .Build();

                context.POIs.AddOrUpdate(c => c.Id, pois.ToArray());

                //Venue Seed
                IdentityUser user = context.Users.First();
                var venues = Builder<Venue>.CreateListOfSize(50)
                .All()
                .With(c => c.Name = Faker.Company.Name())
                .With(c => c.Description = Faker.Lorem.Paragraph(5))
                .With(c => c.Money = Faker.RandomNumber.Next(1, 120))
                .With(c => c.Local = Pick<Local>.RandomItemFrom(locals))
                .With(c => c.OwnerID = user.Id)
                .Build();

                context.Venues.AddOrUpdate(c => c.Id, venues.ToArray());
            }
        }
    }
}
