﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class Local : Base
    {
        [Required]
        [Range(-90, 90, ErrorMessage = "Values must be between -90 and 90")]
        [DisplayName("GPS Latitude")]
        public double GPS_Lat { get; set; }

        [Required]
        [Range(-180, 180, ErrorMessage = "Values must be between -180 and 180")]
        [DisplayName("GPS Longitude")]
        public double GPS_Long { get; set; }

        public ICollection<Venue> Venues { get; set; }
    }
}
