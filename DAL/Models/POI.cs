﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class POI : Base
    {
        [Required]
        [DisplayName("Description")]
        public string Description { get; set; }

        [DisplayName("Local")]
        public Guid LocalID { get; set; }
        public Local Local { get; set; }
    }
}
