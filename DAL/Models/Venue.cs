﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class Venue : Base
    {
        [Required]
        [DisplayName("Description")]
        public string Description { get; set; }

        public bool Entry { get; set; } = true;
        [DisplayName("Entry Value")]
        public decimal Money { get; set; } = 1;

        [DisplayName("Local")]
        public Guid LocalID { get; set; }
        public Local Local { get; set; }

        public string OwnerID { get; set; }
    }
}
