﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Storage
{
    public class GRepository<T> : TRepository, IRepository<T>
    {
        public virtual int Delete(T item)
        {
            var t = typeof(T).Name;
            return db.Execute("DELETE FROM " + t + "s WHERE Id=@Id", item);
    }

        public virtual T FindById(Guid Id)
        {
            var t = typeof(T).Name;
            return db.QuerySingle<T>("SELECT * FROM " + t + "s WHERE Id=@Id");
        }

        /// <summary>
        /// Method to be Overriden
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public virtual int Insert(T item)
        {
            throw new NotImplementedException();
        }

        public virtual ICollection<T> Select()
        {
            var t = typeof(T).Name;
            return db.Query<T>("SELECT * FROM " + t+"s").ToList();
        }

        public virtual ICollection<T> SelectPage(int skip, int take)
        {
            var t = typeof(T).Name;
            return db.Query<T>("SELECT * FROM " + t + "s"+
                    " ORDER BY Id " +
                    " OFFSET " + skip + " ROWS " +
                    " FETCH NEXT " + take + " ROWS ONLY").ToList();
        }

        /// <summary>
        /// Method to be Overriden
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public virtual int Update(T item)
        {
            throw new NotImplementedException();
        }

        public virtual ICollection<T> Where(T item, string WhereCondition)
        {
            throw new NotImplementedException();
        }
    }
}
