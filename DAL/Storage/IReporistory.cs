﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Storage
{
    public interface IRepository<T>
    {
        ICollection<T> Select();
        ICollection<T> SelectPage(int skip, int take);
        T FindById(Guid Id);
        int Insert(T item);
        int Delete(T item);
        int Update(T item);

        ICollection<T> Where(T item, string WhereCondition);
    }
}
