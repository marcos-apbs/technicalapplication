﻿using DAL.Models;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Storage
{
    public class TLocal : GRepository<Local>
    {
        public override int Insert(Local item)
        {
            var sql = "INSERT INTO Locals (Id,Name,GPS_Lat,GPS_Long) Values (@Id, @Name, @GPS_Lat, @GPS_Long);";
            return db.Execute(sql, item);
        }

        public override int Update(Local item)
        {
            var sql = "UPDATE Locals SET Id = @Id, Name=@Name, GPS_Lat=@GPS_Lat, GPS_Long=@GPS_Long WHERE Id=@Id";
            return db.Execute(sql, item);
        }
    }
}
