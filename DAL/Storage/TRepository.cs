﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Storage
{
    public abstract class TRepository : IDisposable
    {
        protected IDbConnection db;

        //WorkArround para coneccao a base de dados sem usar o ApplicationDbContext criado na API
        public TRepository()
        {
            db = new SqlConnection("Data Source=.\\SQLEXPRESS;Initial Catalog=DBProject;Integrated Security=True");
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}
