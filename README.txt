Follow these steps to run the App.

	1 - Run the command "Update-Database" in Package Manager Console to create and seed the database

	2 - Start the App

	3 - Use Postman to make the requests to the API

	4 - Request your Token in /Token with POST
		4.1 - Seeded accounts -	username:	password:
								Founder		foobar
								Client1		foobar
								Client2		foobar

			ex.
			
			Body  
			x-www-form-urlencoded
			KEY					VALUE
			grant_type			password
			username			Founder
			password			foobar

	5 - Make your requests using /api/{controller}
		5.1 - Place the token that was given to you in 4 inside the header

		ex.

		KEY					VALUE
		Authorization		bearer {given token in 4}


