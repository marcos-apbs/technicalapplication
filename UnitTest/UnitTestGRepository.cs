﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DAL.Storage;
using DAL.Models;

namespace UnitTest
{
    /// <summary>
    /// Test Exemple
    /// </summary>
    [TestClass]
    public class UnitTestGRepository
    {
        [TestMethod]
        public void TestSelect()
        {
            //Prepare
            GRepository<Local> tl = new GRepository<Local>();
            GRepository<Venue> tv = new GRepository<Venue>();
            GRepository<POI> tp = new GRepository<POI>();

            //Execute
            var r1 = tl.Select();
            var r2 = tv.Select();
            var r3 = tp.Select();

            //Assert
            Assert.IsNotNull(r1);
            Assert.IsNotNull(r2);
            Assert.IsNotNull(r3);
        }

        [TestMethod]
        public void TestSelectPage()
        {
            //Prepare
            GRepository<Local> tl = new GRepository<Local>();
            GRepository<Venue> tv = new GRepository<Venue>();
            GRepository<POI> tp = new GRepository<POI>();

            //Execute
            var r1 = tl.SelectPage(1,5);
            var r2 = tv.SelectPage(1,5);
            var r3 = tp.SelectPage(1,5);

            //Assert
            Assert.IsNotNull(r1);
            Assert.IsNotNull(r2);
            Assert.IsNotNull(r3);
        }
    }
}
